package edu.ifsp.lojinha.persistencia;

import java.util.List;

import edu.ifsp.lojinha.modelo.Produto;

public class ProdutoDAO {
	List<Produto> produtos = List.of(
			new Produto(1, "Batata"), 
			new Produto(2, "Cebola"), 
			new Produto(3, "Alho")
			);

	public List<Produto> listarTodos() {
		return produtos;
	}

	public Produto findById(int id) {
		for (Produto p : produtos) {
			if (p.getId() == id) {
				return p;
			}
		}
		
		return null;
	}
}
