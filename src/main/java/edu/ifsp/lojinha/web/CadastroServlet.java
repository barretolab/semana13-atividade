package edu.ifsp.lojinha.web;

import java.io.IOException;

import edu.ifsp.lojinha.modelo.Cliente;
import edu.ifsp.lojinha.modelo.Usuario;
import edu.ifsp.lojinha.persistencia.ClienteDAO;
import edu.ifsp.lojinha.persistencia.PersistenceException;
import edu.ifsp.lojinha.persistencia.UsuarioDAO;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/cadastrar")
public class CadastroServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String cmd = request.getParameter("cmd");
		
		if ("exists".equals(cmd)) {
			final String username = request.getParameter("username");
			UsuarioDAO usuarioDao = new UsuarioDAO();
			try {
				boolean exists = usuarioDao.exists(username);
				response.setContentType("text/plain");
				response.getWriter().print(exists);
			} catch (PersistenceException e) {
				throw new ServletException(e);
			}
			return;
		}
		
		/* Havia um comando na URL? */
		if (cmd != null) {
			response.sendRedirect("cadastrar");
		} else {		
			RequestDispatcher rd = request.getRequestDispatcher("cadastro.jsp");
			rd.forward(request, response);
		}		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* mapeando parâmetros do request para objetos do modelo de dados */
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String nome = request.getParameter("nome");
		String email = request.getParameter("email");
		String telefone = request.getParameter("telefone");

		Cliente cliente = new Cliente(nome, email,telefone);
		Usuario usuario = new Usuario();
		usuario.setUsername(username);
		usuario.setPassword(password);
		cliente.setUsuario(usuario);
		cliente.setTelefone(telefone);
		
		/* salvando objetos no banco de dados */
		try {
			UsuarioDAO usuarioDao = new UsuarioDAO();		
			if (!usuarioDao.exists(username)) {
				ClienteDAO dao = new ClienteDAO();
				dao.save(cliente);
				request.setAttribute("saved", true);
			} else {
				request.setAttribute("userExists", true);
			}
		} catch (PersistenceException e) {
			throw new ServletException(e);
		}
		
		request.setAttribute("cliente", cliente);
		request.setAttribute("usuario", usuario);
		
		/* gerando a página de resposta */
		RequestDispatcher rd = request.getRequestDispatcher("cadastro.jsp");
		rd.forward(request, response);		
	}

}
